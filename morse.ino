#include "buzzer.h"
#include "keys.h"

#define LINE_TRESHOLD_MILLIS 200
#define NEW_WORD_TRESHOLD_MILLIS 700
#define SPACE_TRESHOLD_MILLIS 1000

buzzer buzz(12, 500);

key* letter;

bool pressed = false;
bool new_word = true;

double time_pressed = 0;

byte dot_or_line[2] = {
  key::dot_val,
  key::line_val
};

void setup() 
{ 
  Serial.begin(9600);
  key::init();

  letter = key::start_key();
  pinMode(52, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(52))
  {
    new_word = false;
    if(!pressed)
    {
      pressed = true;
      time_pressed = millis();
    }

    buzz.play(30);
  }
  else if(pressed)
  {
    pressed = false;

    if (millis() - time_pressed >= SPACE_TRESHOLD_MILLIS)
    {
      Serial.print(' ');
    }
    
    else if(letter)
    {
      letter = letter->advance(dot_or_line[millis() - time_pressed >= LINE_TRESHOLD_MILLIS]);
    }

    time_pressed = millis();
    
  }
  else if(!new_word)
  {
    if(millis() - time_pressed >= NEW_WORD_TRESHOLD_MILLIS)
    {
      new_word = true;
      Serial.print(letter->get_value());
      letter = key::start_key();
    }
  }
  delay(1);
}
