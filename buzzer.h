class buzzer
{
  int pin;
  int freq;
  
  public:

  buzzer(int pin, int freq) : pin(pin), freq(freq)
  {
    pinMode(pin, OUTPUT);
  }

  inline void change_freq(int freq)
  {
    this->freq = freq;
  }

  void play(int duration_millis)
  {
    double t = millis();

    while((millis() - t) < duration_millis)
    {
      digitalWrite(pin,HIGH);
      delay(1000.0 / freq);//wait for 1ms
      digitalWrite(pin,LOW);
      delay(1000.0 / freq);//wait for 1ms
    }
  }
};
