class key
{
  char value;

  key* dot;
  key* line;

  static key _new_word;
  
  public:

  enum morse_value : byte
  {
    dot_val = 0,
    line_val = 1
  };
  
  enum value_type : char
  {
    new_word = 0,
    placeholder = 0x7f //char max
  };

  key(char val, key* dot, key* line): value(val), dot(dot), line(line) {}
  key(char val) : key(val, nullptr, nullptr){}

  ~key()
  {
    if(dot) delete dot;
    if(line) delete line;
  }

  char get_value()
  {
    if(value == placeholder) return 0;
    return value;
  }

  key* advance(morse_value val)
  {
    if(val == dot_val) return dot;
    if(val == line_val) return line;
  }

  static inline key* start_key()
  {
    return &_new_word;
  }
  static void init()
  {
    _new_word = key(new_word);
    
    _new_word.dot = new key('e');
    _new_word.line = new key('t');
    
    _new_word.dot->dot = new key('i');
    _new_word.dot->line = new key('a');

    _new_word.dot->dot->dot = new key('s');
    _new_word.dot->dot->line = new key('u');

    _new_word.dot->line->dot = new key('r');
    _new_word.dot->line->line = new key('w');

    _new_word.dot->dot->dot->dot = new key('h');
    _new_word.dot->dot->dot->line = new key('v');

    _new_word.dot->dot->line->dot = new key('f');

    _new_word.dot->line->dot->dot = new key('l');

    _new_word.dot->dot->line->line = new key(placeholder);    


    _new_word.dot->line->line->dot = new key('p');
    _new_word.dot->line->line->line = new key('j');

    _new_word.dot->line->line->line->line = new key('1');
    _new_word.dot->dot->line->line->line = new key('2');
    _new_word.dot->dot->dot->line->line = new key('3');

    _new_word.dot->dot->dot->dot->line = new key('4');
    _new_word.dot->dot->dot->dot->dot = new key('5');

    _new_word.line->dot = new key('n');
    _new_word.line->line = new key('m');

    _new_word.line->dot->dot = new key('d');
    _new_word.line->dot->line = new key('k');

    _new_word.line->line->dot = new key('g');
    _new_word.line->line->line = new key('o');

    _new_word.line->dot->dot->dot = new key(placeholder);
    _new_word.line->dot->dot->line = new key('x');

    _new_word.line->dot->line->dot = new key('c');

    _new_word.line->line->dot->dot = new key('z');

    _new_word.line->dot->line->line = new key('y');    


    _new_word.line->line->line->dot = new key(placeholder);
    _new_word.line->line->line->line = new key(placeholder);

    _new_word.line->line->line->line->line = new key('0');
    _new_word.line->line->line->line->dot = new key('9');
    _new_word.line->line->line->dot->dot = new key('8');

    _new_word.line->line->dot->dot->dot = new key('7');
    _new_word.line->dot->dot->dot->dot = new key('6');
    
  }
};

key key::_new_word = key(new_word);
